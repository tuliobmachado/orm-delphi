unit ORM.Atributos;

interface

type
  Tabela = class(TCustomAttribute)
  private
    FNome: string;
  public
    constructor Create(const nome: string);
    property Nome: string read FNome;
  end;

  TCampoProp = (Unique, Required, NoInsert, NoUpdate, Lazy);
  TCampoProps = set of TCampoProp;

  AbstractCampo = class abstract(TCustomAttribute)
  private
    FNome: string;
    FPropriedades: TCampoProps;
  public
    constructor Create(const nome: string); overload; virtual;
    constructor Create(const nome: string; propriedades: TCampoProps); overload; virtual;
    property Nome: string read FNome;
    property Properties: TCampoProps read FPropriedades;
  end;

  Campo = class(AbstractCampo)
  private
    FTamanho: Integer;
    FPrecisao: Integer;
    FEscala: Integer;
  public
    constructor Create(const nome: string); overload; override;
    constructor Create(const nome: string; propriedades: TCampoProps; tamanho: Integer); overload;
    constructor Create(const nome: string; propriedades: TCampoProps; precisao, escala: Integer); overload;
    property Tamanho: Integer read FTamanho;
    property Precisao: Integer read FPrecisao;
    property Escala: Integer read FEscala;
  end;

  TFetchProp = (Lazer, Eager);

  ManyToOne = class(TCustomAttribute)
  public
    constructor Create;
  end;

  OneToMany = class(TCustomAttribute)
  private
    FFetch: TFetchProp;
    FCampoMapeado: String;
  public
    constructor Create(campoMapeado: String); overload;
    constructor Create(campoMapeado: String; fetch: TFetchProp); overload;
    property Fetch: TFetchProp read FFetch;
    property CampoMapeado: String read FCampoMapeado;
  end;

  JoinColuna = class(AbstractCampo)
  private
    FCampoReferenciado: string;
  public
    constructor Create(const nome: string; const campoReferenciado: string); overload;
    property CampoReferenciado: string read FCampoReferenciado;
  end;

  TIdTipo = (Nenhum, IdentityOrSequence);

  Id = class(TCustomAttribute)
  private
    FNomeField: string;
    FIdTipo: TIdTipo;
  public
    constructor Create(const nomeField: string; idTipo: TIdTipo);
    property NomeField: string read FNomeField;
    property IdTipo: TIdTipo read FIdTipo;
  end;

procedure RegistrarEntidade(const Clazz: TClass);

implementation

uses
  System.SysUtils;

procedure RegistrarEntidade(const Clazz: TClass);
begin
  if (Clazz = nil) then
    raise Exception.Create('Erro ao registrar Entidade');
end;

{ Tabela }

constructor Tabela.Create(const nome: string);
begin
  FNome := nome;
end;

{ AbstractCampo }

constructor AbstractCampo.Create(const nome: string; propriedades: TCampoProps);
begin
  Create(nome);
  FPropriedades := propriedades;
end;

constructor AbstractCampo.Create(const nome: string);
begin
  FNome := nome;
end;

{ Campo }

constructor Campo.Create(const nome: string);
begin
  FNome := nome;
end;

constructor Campo.Create(const nome: string; propriedades: TCampoProps;
  tamanho: Integer);
begin
  Create(nome);
  FPropriedades := propriedades;
  FTamanho := tamanho;
end;

constructor Campo.Create(const nome: string; propriedades: TCampoProps;
  precisao, escala: Integer);
begin
  Create(nome);
  FPropriedades := propriedades;
  FPrecisao := precisao;
  FEscala := escala;
end;

{ JoinColuna }

constructor JoinColuna.Create(const nome, campoReferenciado: string);
begin
  FNome := nome;
  FCampoReferenciado := campoReferenciado;
end;

{ ManyToOne }

constructor ManyToOne.Create;
begin
end;

{ OneToMany }

constructor OneToMany.Create(campoMapeado: String; fetch: TFetchProp);
begin
  Create(campoMapeado);
  FFetch := fetch;
end;

constructor OneToMany.Create(campoMapeado: String);
begin
  FCampoMapeado := campoMapeado;
end;

{ Id }

constructor Id.Create(const nomeField: string; idTipo: TIdTipo);
begin
  FNomeField := nomeField;
  FIdTipo := idTipo;
end;

end.
