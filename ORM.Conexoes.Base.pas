unit ORM.Conexoes.Base;

interface

uses
  FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  Data.DB, FireDAC.Comp.Client, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  System.Classes, FireDAC.DApt;

type
  IORMConexao = interface
    ['{A1BA67BF-7869-4FEB-8D12-B89BF5796614}']
    function getConnection: TFDConnection;
  end;

  TORMConexao = class(TInterfacedObject, IORMConexao)
  strict private
  public
    constructor Create(const conexao: TStringList);
    destructor Destroy; override;
    function getConnection: TFDConnection;
  end;

implementation

uses
  ORM.Contexto.Database;

{ TDBConexao }

constructor TORMConexao.Create(const conexao: TStringList);
begin
  FDManager.AddConnectionDef('ORM', 'FB', conexao, False);
  FDManager.ConnectionDefs.ConnectionDefByName('ORM').Params.Pooled := True;

  TORMContextoDatabase.SetCurrent(TORMContextoDatabase.Create());
end;

destructor TORMConexao.Destroy;
begin
  TORMContextoDatabase.FreeContext;
  inherited;
end;

function TORMConexao.getConnection: TFDConnection;
begin
  if (not FDManager.Active) then
    FDManager.Open;

  Result := TFDConnection.Create(nil);
  Result.ConnectionDefName := 'ORM';
end;

end.
