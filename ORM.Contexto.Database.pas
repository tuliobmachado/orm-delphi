unit ORM.Contexto.Database;

interface

uses
  System.Generics.Collections, ORM.MetaDatabase;

type
  TORMContextoDatabase = class
  strict private
    class threadvar _Context: TORMContextoDatabase;
  private
    class var FMetaDatabase: TORMMetaDatabase;
  public
    constructor Create(); reintroduce;
    destructor Destroy; override;
    class procedure SetCurrent(const Value: TORMContextoDatabase);
    class function Current: TORMContextoDatabase;
    class procedure FreeContext();
    function GetMeta(): TORMMetaDatabase;
  end;

implementation

uses
  System.Rtti, ORM.Rtti;

{ TORMContextoDatabase }

constructor TORMContextoDatabase.Create;
var
  ormRtti: IORMRtti;
begin
  ormRtti := TORMRtti.Create;
  FMetaDatabase := ormRtti.obterMetaDatabase();
end;

class function TORMContextoDatabase.Current: TORMContextoDatabase;
begin
  Result := _Context
end;

destructor TORMContextoDatabase.Destroy;
begin
  if (FMetaDatabase <> nil) then
  begin
    FMetaDatabase.Free;
    FMetaDatabase := nil;
  end;
  inherited;
end;

class procedure TORMContextoDatabase.FreeContext;
begin
  _Context.Free;
  _Context := nil;
end;

function TORMContextoDatabase.GetMeta: TORMMetaDatabase;
begin
  Result := FMetaDatabase;
end;

class procedure TORMContextoDatabase.SetCurrent(
  const Value: TORMContextoDatabase);
begin
  _Context := Value;
end;

end.
