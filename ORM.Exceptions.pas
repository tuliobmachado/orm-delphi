unit ORM.Exceptions;

interface

uses
  System.SysUtils;

type
  ENuloValorException = class(Exception)
  public
    constructor Create;
  end;

  ENuloValorException<T> = class(Exception)
  public
    constructor Create;
  end;

implementation

uses
  System.Rtti;

{ ENuloValorException }

constructor ENuloValorException.Create;
begin
  inherited Create('Nulo: N�o pode operar com valor VNull');
end;

{ ENuloValorException<T> }

constructor ENuloValorException<T>.Create;
var
  rContext: TRttiContext;
  strType: string;
begin
  StrType := rContext.GetType(TypeInfo(T)).ToString;

  inherited Create('Nulo: N�o foi poss�vel converter VNull para ' + StrType + '.');
end;

end.
