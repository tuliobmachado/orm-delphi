unit ORM.Gerenciador;

interface

uses
  ORM.Conexoes.Base, ORM.SQLAtributos, System.Generics.Collections;

type
  TORMGerenciador = class
  private
    FConexao: IORMConexao;

    function criarSQLAtributos<E: class>: TORMSQLAtributos<E>;
  protected
    procedure listar(sqlAtributos: TORMSQLAtributos; resultado: TObjectList<TObject>);
    function count(sqlAtributos: TORMSQLAtributos): Integer;
    function having(sqlAtributos: TORMSQLAtributos): Integer;
  public
    constructor Create(conexao: IORMConexao);
    destructor Destroy; override;

    function SQL<E: class>: TORMSQLAtributos<E>;
    procedure salvar(objeto: TObject);
    procedure atualizar(objeto: TObject);
    procedure deletarTudo<E: class>();
    procedure deletarPorId<E: class>(const Id: Variant);
  end;

implementation

{ TGerenciadorObjetos }

uses
  ORM.Query;

procedure TORMGerenciador.atualizar(objeto: TObject);
var
  ORMQuery: IORMQuery;
  ormAtributos: TORMSQLAtributos;
begin
  ormAtributos := TORMSQLAtributos.Create(objeto.ClassType, Self);
  try

    ORMQuery := TORMQuery.Create(FConexao, ormAtributos);
    ORMQuery.atualizar(objeto);
  finally
    ormAtributos.Free;
    ormAtributos := nil;
  end;
end;

function TORMGerenciador.count(sqlAtributos: TORMSQLAtributos): Integer;
var
  ORMQuery: IORMQuery;
begin
  ORMQuery := TORMQuery.Create(FConexao, sqlAtributos);
  Result := ORMQuery.count();
end;

constructor TORMGerenciador.Create(conexao: IORMConexao);
begin
  FConexao := conexao;
end;

function TORMGerenciador.criarSQLAtributos<E>: TORMSQLAtributos<E>;
begin
  Result := TORMSQLAtributos<E>.Create(E, Self);
end;

procedure TORMGerenciador.deletarPorId<E>(const Id: Variant);
var
  ORMQuery: IORMQuery;
begin
  ORMQuery := TORMQuery.Create(FConexao, TORMSQLAtributos<E>.Create(E, Self));
  ORMQuery.deletarPorId(Id);
end;

procedure TORMGerenciador.deletarTudo<E>;
var
  ORMQuery: IORMQuery;
  ORMSQLAtt: TORMSQLAtributos;
begin
  ORMSQLAtt := TORMSQLAtributos<E>.Create(E, Self);
  try
    ORMQuery := TORMQuery.Create(FConexao, ORMSQLAtt);
    ORMQuery.deletarTudo();
  finally
    ORMSQLAtt.Free;
    ORMSQLAtt := nil;
  end;
end;

destructor TORMGerenciador.Destroy;
begin

  inherited;
end;

function TORMGerenciador.having(sqlAtributos: TORMSQLAtributos): Integer;
var
  ORMQuery: IORMQuery;
begin
  ORMQuery := TORMQuery.Create(FConexao, sqlAtributos);
  Result := ORMQuery.having();
end;

procedure TORMGerenciador.listar(sqlAtributos: TORMSQLAtributos;
  resultado: TObjectList<TObject>);
var
  ORMQuery: IORMQuery;
begin
  ORMQuery := TORMQuery.Create(FConexao, sqlAtributos);
  while ORMQuery.listarRegistros do
    resultado.Add(ORMQuery.obterRegistro);
end;

procedure TORMGerenciador.salvar(objeto: TObject);
var
  ORMQuery: IORMQuery;
  ORMSQLAtt: TORMSQLAtributos;
begin
  ORMSQLAtt := TORMSQLAtributos.Create(objeto.ClassType, Self);
  try
    ORMQuery := TORMQuery.Create(FConexao, ORMSQLAtt);
    ORMQuery.salvar(objeto);
  finally
    ORMSQLAtt.Free;
    ORMSQLAtt := nil;
  end;
end;

function TORMGerenciador.SQL<E>: TORMSQLAtributos<E>;
begin
  Result := criarSQLAtributos<E>;
end;

end.
