unit ORM.MetaDatabase;

interface

uses
  System.Generics.Collections, System.Rtti, ORM.Atributos;

type

  TORMMetaCampo = class
  strict private
    FNome: String;
    FNomeField: String;
    FNomeCampoRelacionamento: String;
    FFetch: TFetchProp;
    FCampoSelect: String;
  published
    property Nome: String read FNome write FNome;
    property NomeField: String read FNomeField write FNomeField;
    property NomeCampoRelacionamento: String read FNomeCampoRelacionamento write FNomeCampoRelacionamento;
    property CampoSelect: String read FCampoSelect write FCampoSelect;
    property Fetch: TFetchProp read FFetch write FFetch;
  end;

  TORMMetaTabela = class
  private

  strict private
    FNomeClasse: String;
    FNomeQualified: String;
    FNomeTabela: String;
    FNomeCampoId: String;
    FIdTipo: TIdTipo;
    FCampos: TObjectList<TORMMetaCampo>;
  public
    destructor Destroy; override;
  published
    property NomeClasse: String read FNomeClasse write FNomeClasse;
    property NomeQualified: String read FNomeQualified write FNomeQualified;
    property NomeTabela: String read FNomeTabela write FNomeTabela;
    property NomeCampoId: String read FNomeCampoId write FNomeCampoId;
    property IdTipo: TIdTipo read FIdTipo write FIdTipo;
    property Campos: TObjectList<TORMMetaCampo> read FCampos write FCampos;
  end;

  TORMMetaDatabase = class
  private
    FTabelas: TObjectList<TORMMetaTabela>;
  public
    constructor Create();
    destructor Destroy; override;

    procedure adicionarTabela(tabela: TORMMetaTabela);
    function obterCampos(classe: TClass): String;
    function obterNomeTabela(classe: TClass): String;
    function obterFieldId(classe: TClass): String;
    function obterFieldIdNomeCampo(classe: TClass): String;
    function obterFieldPorNomeCampo(classe: TClass; nomeCampo: String): String;
    function obterFieldRelacionamentoPorNomeCampo(classe: TClass; nomeCampo: String): String;
    function obterNomeCampoPorField(classe: TClass; nomeField: string): String;
  end;

implementation

uses
  System.SysUtils;

{ TORMMetaDatabase }

destructor TORMMetaTabela.Destroy;
begin
  if (FCampos <> nil) then
  begin
    FCampos.Free;
    FCampos := nil;
  end;
  inherited;
end;

{ TORMMetaDatabase }

procedure TORMMetaDatabase.adicionarTabela(tabela: TORMMetaTabela);
begin
  FTabelas.Add(tabela);
end;

constructor TORMMetaDatabase.Create;
begin
  FTabelas := TObjectList<TORMMetaTabela>.Create;
end;

destructor TORMMetaDatabase.Destroy;
begin
  if (FTabelas <> nil) then
  begin
    FTabelas.Free;
    FTabelas := nil;
  end;
  inherited;
end;

function TORMMetaDatabase.obterCampos(classe: TClass): String;
var
  tabela: TORMMetaTabela;
  campo: TORMMetaCampo;
begin
  Result := '';

  for tabela in FTabelas do
  begin
    if (tabela.NomeQualified = classe.QualifiedClassName) then
    begin
      for campo in tabela.Campos do
      begin
        if (Result <> '') then
          Result := Result + ', ';

        Result := Result + campo.CampoSelect;
      end;
    end;
  end;
end;

function TORMMetaDatabase.obterFieldId(classe: TClass): String;
var
  tabela: TORMMetaTabela;
begin
  for tabela in FTAbelas do
  begin
    if (tabela.NomeQualified = classe.QualifiedClassName) then
    begin
      Result := tabela.NomeCampoId;
      Break;
    end;
  end;
end;

function TORMMetaDatabase.obterFieldIdNomeCampo(classe: TClass): String;
var
  tabela: TORMMetaTabela;
  campo: TORMMetaCampo;
  nomeCampo: String;
begin
  for tabela in FTAbelas do
  begin
    if (tabela.NomeQualified = classe.QualifiedClassName) then
    begin
      nomeCampo := tabela.NomeCampoId;

      for campo in tabela.Campos do
      begin
        if (AnsiLowerCase(campo.NomeField) = AnsiLowerCase(nomeCampo)) then
        begin
          Result := campo.Nome;
          Break;
        end;
      end;

      Break;
    end;
  end;
end;

function TORMMetaDatabase.obterFieldPorNomeCampo(classe: TClass;
  nomeCampo: String): String;
var
  tabela: TORMMetaTabela;
  campo: TORMMetaCampo;
begin
  Result := '';

  for tabela in FTabelas do
  begin
    if (tabela.NomeQualified = classe.QualifiedClassName) then
    begin
      for campo in tabela.Campos do
      begin
        if (AnsiLowerCase(campo.Nome) = AnsiLowerCase(nomeCampo)) then
        begin
          Result := campo.NomeField;
          break;
        end;
      end;
      break;
    end;
  end;

end;

function TORMMetaDatabase.obterFieldRelacionamentoPorNomeCampo(classe: TClass;
  nomeCampo: String): String;
var
  tabela: TORMMetaTabela;
  campo: TORMMetaCampo;
begin
  Result := '';

  for tabela in FTabelas do
  begin
    if (tabela.NomeQualified = classe.QualifiedClassName) then
    begin
      for campo in tabela.Campos do
      begin
        if (AnsiLowerCase(campo.Nome) = AnsiLowerCase(nomeCampo)) then
        begin
          Result := campo.NomeCampoRelacionamento;
          break;
        end;
      end;
      break;
    end;
  end;
end;

function TORMMetaDatabase.obterNomeCampoPorField(classe: TClass;
  nomeField: string): String;
var
  tabela: TORMMetaTabela;
  campo: TORMMetaCampo;
begin
  Result := '';

  for tabela in FTabelas do
  begin
    if (tabela.NomeQualified = classe.QualifiedClassName) then
    begin
      for campo in tabela.Campos do
      begin
        if (AnsiLowerCase(campo.NomeField) = AnsiLowerCase(nomeField)) then
        begin
          Result := campo.Nome;
          break;
        end;
      end;
      break;
    end;
  end;
end;

function TORMMetaDatabase.obterNomeTabela(classe: TClass): String;
var
  tabela: TORMMetaTabela;
begin
  for tabela in FTAbelas do
  begin
    if (tabela.NomeQualified = classe.QualifiedClassName) then
    begin
      Result := tabela.NomeTabela;
      Break;
    end;
  end;
end;

end.
