unit ORM.Query;

interface

uses
  ORM.SQLAtributos, FireDAC.Comp.Client, ORM.Conexoes.Base,
  System.Generics.Collections, System.Classes;

type
  TArrayVariant = array of Variant;

  IORMQuery = interface
    ['{308664E5-69BA-4113-82F4-E1FC18647AF9}']

    function listarRegistros(): boolean;
    function obterRegistro(): TObject;
    function count(): Integer;
    function having(): Integer;
    function first(): TObject;
    procedure salvar(objeto: TObject);
    procedure atualizar(objeto: TObject);
    procedure deletarTudo();
    procedure deletarPorId(const Id: Variant);
  end;

  TORMQuery = class(TInterfacedObject, IORMQuery)
  private
    FAtributos: TORMSQLAtributos;
    FConnection: TFDConnection;
    FQuerys: TObjectList<TFDQuery>;
    FRegistroAtual: TObject;
    FLog: TStringList;
  private
    function setarParametros(dicionario: TDictionary<String,Variant>): TArrayVariant;
    function obterRegistroAtual(): TObject;
    function montarSQLHaving(): String;
    function montarSQLCount(): String;
    function montarSQL(): String;
    procedure salvarLog(const sql: String; mensagem: String; codigo: String;
      parametros: TStrings);
  public
    constructor Create(conexao: IORMConexao; sqlAtributos: TORMSQLAtributos);
    destructor Destroy; override;

    function listarRegistros(): boolean;
    function obterRegistro(): TObject;
    function getClasse(): TClass;
    function getQueryRelacionamento(const nomeQuery: String;
      nomeCampoRelacionamento: String; valor: Variant; classe: TClass): TFDQuery;
    function getQueryPrincipal(): TFDQuery;
    function count(): Integer;
    function having(): Integer;
    function first(): TObject;
    procedure salvar(objeto: TObject);
    procedure atualizar(objeto: TObject);
    procedure deletarTudo();
    procedure deletarPorId(const id: Variant);
  end;

implementation

uses
  System.SysUtils, ORM.Contexto.Database, ORM.Rtti, FireDAC.Stan.Error,
  ACBrUtil;

{ TORMQuery }

procedure TORMQuery.atualizar(objeto: TObject);
var
  vRtti: IORMRtti;
begin
  vRtti := TORMRtti.Create;
  getQueryPrincipal.Close;
  getQueryPrincipal.SQL.Add(vRtti.obterSQLUpdate(objeto));
  setarParametros(vRtti.getDicionarioValores());

  try
    getQueryPrincipal.ExecSQL();
  except
    on E: EFDDBEngineException do
    begin
      salvarLog(E.SQL, E.Message, e.ErrorCode.ToString, E.Params);
      raise;
    end;
  end;
end;

function TORMQuery.count: Integer;
begin
  getQueryPrincipal.Open(montarSQLCount());
  Result := getQueryPrincipal.Fields[0].AsInteger;
end;

constructor TORMQuery.Create(conexao: IORMConexao; sqlAtributos: TORMSQLAtributos);
begin
  FAtributos := sqlAtributos;
  FConnection := conexao.getConnection;
  FQuerys := TObjectList<TFDQuery>.Create;
  FQuerys.Add(TFDQuery.Create(nil));
  FQuerys[FQuerys.Count-1].Name := TORMContextoDatabase.Current.GetMeta.obterNomeTabela(sqlAtributos.getClasse);
  FQuerys[FQuerys.Count-1].Connection := FConnection;
  FLog := TStringList.Create;
end;

procedure TORMQuery.deletarPorId(const id: Variant);
var
  vSQL: String;
begin
  vSQL := 'DELETE FROM ' + TORMContextoDatabase.Current.GetMeta.obterNomeTabela(FAtributos.getClasse()) +
  ' WHERE ' + TORMContextoDatabase.Current.GetMeta.obterFieldIdNomeCampo(FAtributos.getClasse()) + ' = :CAMPO';
  getQueryPrincipal.ExecSQL(vSQL, [id]);
end;

procedure TORMQuery.deletarTudo;
var
  vSQL: String;
begin
  vSQL := 'DELETE FROM '+ TORMContextoDatabase.Current.GetMeta.obterNomeTabela(FAtributos.getClasse());
  getQueryPrincipal.ExecSQL(vSQL);
end;

destructor TORMQuery.Destroy;
begin
  if (FConnection <> nil) then
  begin
    FConnection.Free;
    FConnection := nil;
  end;

  if (FQuerys <> nil) then
  begin
    FQuerys.Free;
    FQuerys := nil;
  end;

  if (FLog <> nil) then
  begin
    FLog.Free;
    FLog := nil;
  end;

  inherited;
end;

function TORMQuery.first: TObject;
begin
  Result := FRegistroAtual;
end;

function TORMQuery.getClasse: TClass;
begin
  Result := FAtributos.getClasse;
end;

function TORMQuery.getQueryRelacionamento(const nomeQuery: String;
  nomeCampoRelacionamento: String; valor: Variant; classe: TClass): TFDQuery;
var
  I: Integer;
begin
  Result := nil;

  for I := 0 to FQuerys.Count-1 do
  begin
    if (FQuerys[I].Name = nomeQuery) then
    begin
      FQuerys[I].Close;
      FQuerys[I].Params[0].Value := valor;
      FQuerys[I].Open;
      Result := FQuerys[I];
      Break;
    end;
  end;

  if (Result = nil) then
  begin
    FQuerys.Add(TFDQuery.Create(nil));
    FQuerys[FQuerys.Count-1].Name := nomeQuery;
    FQuerys[FQuerys.Count-1].Connection := FConnection;
    FQuerys[FQuerys.Count-1].SQL.Add('SELECT DISTINCT '+
      TORMContextoDatabase.Current.GetMeta.obterCampos(classe) + ' FROM '+
      TORMContextoDatabase.Current.GetMeta.obterNomeTabela(classe) + ' WHERE '+
      nomeCampoRelacionamento +' = :nomeCampoRelacionamento');
    FQuerys[FQuerys.Count-1].Params[0].Value := valor;
    FQuerys[FQuerys.Count-1].Open;
    Result := FQuerys[FQuerys.Count-1];
  end;
end;

function TORMQuery.having: Integer;
begin
  Result := 0;
  getQueryPrincipal.Open(montarSQLHaving());

  if (getQueryPrincipal.RecordCount > 0) then
    Result := getQueryPrincipal.FieldByName('TOTAL').AsInteger;
end;

function TORMQuery.getQueryPrincipal: TFDQuery;
begin
  Result := FQuerys[0];
end;

function TORMQuery.listarRegistros: boolean;
begin
  if (not getQueryPrincipal.Active) then
  begin
    getQueryPrincipal.Open(montarSQL());
    getQueryPrincipal.First;
  end;

  Result := not getQueryPrincipal.Eof;

  if (Result) then
    FRegistroAtual := obterRegistroAtual();
end;

function TORMQuery.obterRegistroAtual: TObject;
var
  rtti: IORMRtti;
begin
  rtti := TORMRtti.Create;
  Result := rtti.queryToObject(Self);
  getQueryPrincipal.Next;
end;

procedure TORMQuery.salvar(objeto: TObject);
var
  vRtti: IORMRtti;
begin
  vRtti := TORMRtti.Create;
  getQueryPrincipal.Close;
  getQueryPrincipal.SQL.Add(vRtti.obterSQLInsert(objeto));
  setarParametros(vRtti.getDicionarioValores());

  try
    getQueryPrincipal.ExecSQL();
  except
    on E: EFDDBEngineException do
    begin
      salvarLog(E.SQL, E.Message, e.ErrorCode.ToString, E.Params);
      raise;
    end;
  end;
end;

procedure TORMQuery.salvarLog(const sql: String; mensagem: String;
  codigo: String; parametros: TStrings);
var
  nomeArquivo: String;
  valores: String;
  i: Integer;
begin
  nomeArquivo := PathWithDelim(ExtractFilePath(ParamStr(0))) + 'log' +
    PathDelim + 'ORM-' + FormatDateTime('ddmmyyyy', Now) + '.log';

  if (parametros.Count > 0) then
  begin
    for i := 0 to parametros.Count -1 do
    begin
      if (valores <> '') then
        valores := valores + ', ';

      valores := valores + parametros[i];
    end;
  end;

  with TStringList.Create do
  try
    if FileExists(NomeArquivo) then
      LoadFromFile(NomeArquivo);
    Add('Data/Hora: [' + DateTimeToStr(Now) + ']');
    Add('Origem: [ORM]');
    Add('C�digo Erro: [' + codigo + ']');
    Add('SQL: [' + sql + ']');
    Add('Valores: [' + valores + ']');
    Add('Erro: [' + mensagem + ']');
    Add('-------');
    SaveToFile(NomeArquivo);
  finally
    Free;
  end;
end;

function TORMQuery.setarParametros(dicionario: TDictionary<String,Variant>): TArrayVariant;
var
  key: String;
begin
  for key in dicionario.Keys do
  begin
    if (getQueryPrincipal.Params.FindParam(key) <> nil) then
      getQueryPrincipal.Params.ParamByName(key).Value := dicionario.Items[key];
  end;
end;

function TORMQuery.montarSQL: String;
var
  vSQL: String;
  vFirst: String;
begin
  if (FAtributos.getFirst()) then
    vFirst := ' FIRST 1 ';

  if Trim(FAtributos.getFields) <> '' then
    vSQL := vSQL + ' SELECT ' + FAtributos.getFields
  else
    vSQL := vSQL + ' SELECT ' + vFirst + TORMContextoDatabase.Current.GetMeta.obterCampos(FAtributos.getClasse);

  vSQL := vSQL + ' FROM ' + TORMContextoDatabase.Current.GetMeta.obterNomeTabela(FAtributos.getClasse);

  if Trim(FAtributos.getJoin) <> '' then
    vSQL := vSQL + ' ' + FAtributos.getJoin + ' ';
  if Trim(FAtributos.getWhere) <> '' then
    vSQL := vSQL + ' WHERE ' + FAtributos.getWhere;
  if Trim(FAtributos.getGroupBy) <> '' then
    vSQL := vSQL + ' GROUP BY ' + FAtributos.getGroupBy;
  if Trim(FAtributos.getOrderBy) <> '' then
    vSQL := vSQL + ' ORDER BY ' + FAtributos.getOrderBy;

  Result := vSQL;
end;

function TORMQuery.montarSQLCount: String;
var
  vSQL: String;
begin
  vSQL := 'SELECT COUNT(*) FROM ' + TORMContextoDatabase.Current.GetMeta.obterNomeTabela(FAtributos.getClasse);

  if Trim(FAtributos.getJoin) <> '' then
    vSQL := vSQL + ' ' + FAtributos.getJoin + ' ';

  if Trim(FAtributos.getWhere) <> '' then
    vSQL := vSQL + ' WHERE ' + FAtributos.getWhere;

  Result := vSQL;
end;

function TORMQuery.montarSQLHaving: String;
var
  vSQL: String;
begin
  vSQL := 'SELECT ' + FAtributos.getFields + ', COUNT( ' +
    FAtributos.getFields + ' ) AS TOTAL FROM ' + TORMContextoDatabase.Current.GetMeta.obterNomeTabela(FAtributos.getClasse);

  if Trim(FAtributos.getWhere) <> '' then
    vSQL := vSQL + ' WHERE ' + FAtributos.getWhere;

  if Trim(FAtributos.getGroupBy) <> '' then
    vSQL := vSQL + ' GROUP BY ' + FAtributos.getGroupBy;

  vSQL := vSQL + ' HAVING COUNT( ' + FAtributos.getFields + ' ) > 1 ';

  Result := vSQL;
end;

function TORMQuery.obterRegistro: TObject;
begin
  Result := FRegistroAtual;
end;

end.
