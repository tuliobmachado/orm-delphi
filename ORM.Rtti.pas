unit ORM.Rtti;

interface

uses
  ORM.MetaDatabase, FireDAC.Comp.Client, System.Rtti,
  System.Generics.Collections, System.TypInfo, ORM.Query, Data.DB;


type
  IORMRtti = interface
  ['{A7F0795D-F5E1-46C9-A8BC-D7AEE0B420D3}']
    function obterMetaDatabase: TORMMetaDatabase;
    function queryToObject(query: TORMQuery): TObject;
    function obterSQLInsert(objeto: TObject): String;
    function obterSQLUpdate(objeto: TObject): String;
    function getDicionarioValores(): TDictionary<String, Variant>;
  end;

  TORMRtti = class(TInterfacedObject, IORMRtti)
  private
    FQuery: TORMQuery;
    FDicionarioValores: TDictionary<String, Variant>;
    FDicionarioCampos: TDictionary<String, Variant>;
    FWhereChavePrimaria: String;

    function isAtributo<E: TCustomAttribute>(atributo: TCustomAttribute): Boolean;
    function getAtributo<E: TCustomAttribute>(atributo: TCustomAttribute): E;

    function carregarCampos(nomeTabela: String; rType: TRttiType): TObjectList<TORMMetaCampo>;

    function instanciarObjeto(classe: TClass): TObject;
    function setarValorTipoNulo(typeInfo: PTypeInfo; valorField: Variant): TValue;
    function setarValorTipoRelacionamento(classe: TClass; nomeCampo: String;
      typeInfo: PTypeInfo; valorField: Variant): TValue;
    function isTypeNulo(rType: TRttiType): boolean;
    function isTypeRelacionamento(rType: TRttiType): Boolean;
    procedure obterValue(classe: TClass; field: TField; rField: TRttiField;
      var Result: TObject; var value: TValue);
    function _FloatFormat(valor: String ): Currency;
    function obterValorFieldToVariant(rField: TRttiField; value: TValue): Variant;
    function obterValorTipoNulo(value: TValue; rttiRecord: TRttiRecordType): Variant;
    function obterValorTipoRelacionamentoVariant(const nomeCampoRelacionamento: String;
      objeto: TObject): Variant;
    function obterValorTipoRelacionamento(nomeCampo: String; value: TValue;
      rttiRecord: TRttiRecordType): Variant;
    function obterFields(const inserindo: Boolean = True): String;
    function obterFieldsParam(): String;
    procedure obterCamposEValores(objeto: TObject);
  public
    constructor Create();
    destructor Destroy; override;

    function obterMetaDatabase: TORMMetaDatabase;
    function queryToObject(query: TORMQuery): TObject;
    function obterSQLInsert(objeto: TObject): string;
    function obterSQLUpdate(objeto: TObject): String;
    function getDicionarioValores: TDictionary<System.string,System.Variant>;
  end;

  TORMRtti<E: class> = class(TORMRtti)

  end;

implementation

{ TORMRtti }

uses
  ORM.Atributos, System.Variants, ORM.Contexto.Database,
  System.SysUtils;

function TORMRtti.obterFields(const inserindo: Boolean = True): String;
var
  key: string;
begin
  for key in FDicionarioCampos.Keys do
  begin
    if (Result <> '') then
      Result := Result + ', ';

    if (inserindo) then
      Result := Result + key
    else
      Result := Result + key + ' = :' + key;
  end;
end;

function TORMRtti.obterFieldsParam(): String;
var
  key: string;
begin
  for key in FDicionarioCampos.Keys do
  begin
    if (Result <> '') then
      Result := Result + ', ';

    Result := Result + ':' + key;
  end;
end;

function TORMRtti.carregarCampos(nomeTabela: String; rType: TRttiType): TObjectList<TORMMetaCampo>;
var
  customAttribute: TCustomAttribute;
  rField: TRttiField;
  campoMeta: TORMMetaCampo;
begin
  Result := TObjectList<TORMMetaCampo>.Create;

  for rField in rType.GetFields do
  begin
    for customAttribute in rField.GetAttributes do
    begin
      if (isAtributo<ORM.Atributos.Campo>(customAttribute)) then
      begin
        campoMeta := TORMMetaCampo.Create;
        campoMeta.Nome := getAtributo<ORM.Atributos.Campo>(customAttribute).Nome;
        campoMeta.NomeField := rField.Name;
        campoMeta.CampoSelect := nomeTabela+'.'+campoMeta.Nome;

        Result.Add(campoMeta);
      end;

      if (isAtributo<ORM.Atributos.JoinColuna>(customAttribute)) then
      begin
        campoMeta := TORMMetaCampo.Create;
        campoMeta.Nome := getAtributo<ORM.Atributos.JoinColuna>(customAttribute).Nome;
        campoMeta.NomeCampoRelacionamento := getAtributo<ORM.Atributos.JoinColuna>(customAttribute).CampoReferenciado;
        campoMeta.NomeField := rField.Name;
        campoMeta.CampoSelect := nomeTabela+'.'+campoMeta.Nome;

        Result.Add(campoMeta);
      end;

      if (isAtributo<ORM.Atributos.OneToMany>(customAttribute)) then
      begin
        campoMeta := TORMMetaCampo.Create;
        campoMeta.Nome := getAtributo<ORM.Atributos.OneToMany>(customAttribute).CampoMapeado;
        campoMeta.Fetch := getAtributo<ORM.Atributos.OneToMany>(customAttribute).Fetch;
        campoMeta.NomeField := rField.Name;

        Result.Add(campoMeta);
      end;

    end;
  end;
end;

constructor TORMRtti.Create;
begin
  FDicionarioValores := TDictionary<String, Variant>.Create;
  FDicionarioCampos := TDictionary<String, Variant>.Create;
end;

destructor TORMRtti.Destroy;
begin
  if (FDicionarioValores <> nil) then
  begin
    FDicionarioValores.Free;
    FDicionarioValores := nil;
  end;

  if (FDicionarioCampos <> nil) then
  begin
    FDicionarioCampos.Free;
    FDicionarioCampos := nil;
  end;

  inherited;
end;

function TORMRtti.getAtributo<E>(atributo: TCustomAttribute): E;
begin
  Result := atributo as E;
end;

function TORMRtti.getDicionarioValores: TDictionary<System.string, System.Variant>;
begin
  Result := FDicionarioValores
end;

procedure TORMRtti.obterCamposEValores(objeto: TObject);
var
  rType: TRttiType;
  rContext: TRttiContext;
  rField: TRttiField;
  campoChave: string;
  nomeCampo: string;
begin
  rType := rContext.GetType(objeto.ClassType);
  campoChave := TORMContextoDatabase.Current.GetMeta.obterFieldIdNomeCampo(objeto.ClassType);
  FWhereChavePrimaria := campoChave + ' = :'+ campoChave;
  for rField in rType.GetFields do
  begin
    nomeCampo := TORMContextoDatabase.Current.GetMeta.obterNomeCampoPorField(objeto.ClassType, rField.Name);
    FDicionarioCampos.Add(nomeCampo, nomeCampo);
    case rField.FieldType.TypeKind of
      tkInteger, tkInt64:
        FDicionarioValores.Add(nomeCampo, rField.GetValue(Pointer(objeto)).AsInteger);
      tkFloat:
      begin
        if (rField.GetValue(Pointer(objeto)).TypeInfo = TypeInfo(TDateTime)) then
          FDicionarioValores.Add(nomeCampo, StrToDateTime(rField.GetValue(Pointer(objeto)).ToString))
        else if (rField.GetValue(Pointer(objeto)).TypeInfo = TypeInfo(TDate)) then
          FDicionarioValores.Add(nomeCampo, StrToDate(rField.GetValue(Pointer(objeto)).ToString))
        else if (rField.GetValue(Pointer(objeto)).TypeInfo = TypeInfo(TTime)) then
          FDicionarioValores.Add(nomeCampo, StrToTime(rField.GetValue(Pointer(objeto)).ToString))
        else
          FDicionarioValores.Add(nomeCampo, _FloatFormat(rField.GetValue(Pointer(objeto)).ToString));
      end;
      tkWChar, tkLString, tkWString, tkUString, tkString:
        FDicionarioValores.Add(nomeCampo, rField.GetValue(Pointer(objeto)).AsString);
      tkVariant:
        FDicionarioValores.Add(nomeCampo, rField.GetValue(Pointer(objeto)).AsVariant);
      tkRecord:
      begin
        if (isTypeNulo(rField.FieldType)) then
          FDicionarioValores.Add(nomeCampo, obterValorTipoNulo(rField.GetValue(Pointer(objeto)), rContext.GetType(rField.GetValue(Pointer(objeto)).TypeInfo).AsRecord));

        if (isTypeRelacionamento(rField.FieldType)) then
          FDicionarioValores.Add(nomeCampo,
            obterValorTipoRelacionamento(
              TORMContextoDatabase.Current.GetMeta.obterFieldRelacionamentoPorNomeCampo(objeto.ClassType, nomeCampo),
              rField.GetValue(Pointer(objeto)),
              rContext.GetType(rField.GetValue(Pointer(objeto)).TypeInfo).AsRecord));
      end;
    else
      FDicionarioValores.Add(nomeCampo, rField.GetValue(Pointer(objeto)).AsString);
    end;
  end;
end;

function TORMRtti.instanciarObjeto(classe: TClass): TObject;
var
  rType: TRttiType;
  mType: TRTTIMethod;
  rContext: TRttiContext;
begin
  rType := rContext.GetType(classe);  
  for mType in rType.GetMethods do
  begin
    if mType.HasExtendedInfo and mType.IsConstructor then
    begin
      if Length(mType.GetParameters) = 0 then
      begin
        // invoke
        Exit(mType.Invoke(classe, []).AsObject);
      end;
    end;
  end;
end;

function TORMRtti.isAtributo<E>(atributo: TCustomAttribute): Boolean;
begin
  Result := atributo is E;
end;

function TORMRtti.isTypeNulo(rType: TRttiType): boolean;
begin
  Result := (rType is TRttiRecordType) and (Pos('ORM.Tipos.Nulo', rType.QualifiedName) > 0);
end;

function TORMRtti.isTypeRelacionamento(rType: TRttiType): Boolean;
begin
  Result := (rType is TRttiRecordType) and (Pos('ORM.Tipos.Relacionamento', rType.QualifiedName) > 0);
end;

function TORMRtti.obterMetaDatabase: TORMMetaDatabase;
var
  rContext: TRttiContext;
  rType: TRttiType;
  customAttribute: TCustomAttribute;
  metaTabela: TORMMetaTabela;
begin
  Result := TORMMetaDatabase.Create();

  for rType in rContext.GetTypes do
  begin
    for customAttribute in rType.GetAttributes do
    begin
      if (isAtributo<ORM.Atributos.Tabela>(customAttribute)) then
      begin
        metaTabela := TORMMetaTabela.Create;
        metaTabela.NomeClasse := rType.Name;
        metaTabela.NomeQualified := rType.QualifiedName;
        metaTabela.NomeTabela := getAtributo<ORM.Atributos.Tabela>(customAttribute).Nome;
        metaTabela.Campos := carregarCampos(metaTabela.NomeTabela, rType);
      end;

      if (isAtributo<ORM.Atributos.Id>(customAttribute)) then
      begin
        metaTabela.NomeCampoId := getAtributo<ORM.Atributos.Id>(customAttribute).nomeField;
        metaTabela.IdTipo := getAtributo<ORM.Atributos.Id>(customAttribute).IdTipo;

        Result.adicionarTabela(metaTabela);
      end;
    end;
  end;
end;

function TORMRtti.obterSQLInsert(objeto: TObject): string;
begin
  obterCamposEValores(objeto);

  Result := Result + 'INSERT INTO ' +
  TORMContextoDatabase.Current.GetMeta.obterNomeTabela(objeto.ClassType) + ' ';
  Result := Result + ' (' + obterFields() + ') ';
  Result := Result + ' VALUES ';
  Result := Result + ' (' + obterFieldsParam() + ')';
end;

function TORMRtti.obterSQLUpdate(objeto: TObject): String;
begin
  obterCamposEValores(objeto);

  Result := Result + 'UPDATE ' +
  TORMContextoDatabase.Current.GetMeta.obterNomeTabela(objeto.ClassType) + ' ';
  Result := Result + ' SET ' + obterFields(False);
  Result := Result + ' WHERE ' + FWhereChavePrimaria;
end;

function TORMRtti.setarValorTipoNulo(typeInfo: PTypeInfo;
  valorField: Variant): TValue;
var
  rContext: TRttiContext;
  rType: TRttiType;
  valorReal: TValue;
  rValorField: TRttiField;
  rPossuiValorField: TRttiField;
  isValorNulo: Boolean;  
begin
  rType := rContext.GetType(typeInfo);
  rValorField := rType.GetField('FValue');
  rPossuiValorField := rType.GetField('FPossuiValor');
  isValorNulo := VarIsNull(valorField);

  TValue.Make(nil, rType.Handle, Result);

  if (isValorNulo) then
    rPossuiValorField.SetValue(Result.GetReferenceToRawData, False)
  else
  begin  
    case rValorField.FieldType.TypeKind of
      tkUnknown, tkString, tkWChar, tkLString, tkWString, tkUString:
        valorReal := TValue.From<String>(valorField);
      tkInteger, tkInt64:
        valorReal := TValue.From<Integer>(valorField);
      tkFloat: valorReal := TValue.From<Double>(valorField);
    end;

    rPossuiValorField.SetValue(Result.GetReferenceToRawData, True);
    rValorField.SetValue(Result.GetReferenceToRawData, valorReal);
  end;
end;

function TORMRtti.setarValorTipoRelacionamento(classe: TClass; nomeCampo: String;
  typeInfo: PTypeInfo; valorField: Variant): TValue;
var
  rContext: TRttiContext;
  rType: TRttiType;
  rTypeField: TRttiType;
  rValorField: TRttiField;
  vQuery: TFDQuery;
  field: TField;
  rField: TRttiField;
  Results: TObject;
  value: TValue;
begin
  rType := rContext.GetType(typeInfo);
  rValorField := rType.GetField('FValue');
  vQuery := FQuery.getQueryRelacionamento(
    TORMContextoDatabase.Current.GetMeta.obterNomeTabela(classe)+'_'+TORMContextoDatabase.Current.GetMeta.obterNomeTabela(rValorField.FieldType.AsInstance.MetaclassType),
    TORMContextoDatabase.Current.GetMeta.obterFieldRelacionamentoPorNomeCampo(classe, nomeCampo),
    valorField,
    rValorField.FieldType.AsInstance.MetaclassType
  );

  TValue.Make(nil, rType.Handle, Result);

  if (not vQuery.IsEmpty) then
  begin
    Results := instanciarObjeto(rValorField.FieldType.AsInstance.MetaclassType);
    rTypeField := rContext.GetType(rValorField.FieldType.AsInstance.MetaclassType);

    for field in vQuery.Fields do
    begin
      rField := rTypeField.GetField(TORMContextoDatabase.Current.GetMeta.obterFieldPorNomeCampo(rValorField.FieldType.AsInstance.MetaclassType, AnsiLowerCase(field.DisplayName)));

      if (rField <> nil) then
      begin
        obterValue(rValorField.FieldType.AsInstance.MetaclassType, field, rField, Results, value);

        rField.SetValue(TObject(Results), value);
      end;
    end;

    rValorField.SetValue(Result.GetReferenceToRawData, TValue.From(Results));
  end;
end;

function TORMRtti.queryToObject(query: TORMQuery): TObject;
var
  classe: TClass;
  field: TField;
  rContext: TRttiContext;
  rType: TRttiType;
  rField: TRttiField;
  value: TValue;
begin
  FQuery := query;
  classe := FQuery.getClasse;
  Result := instanciarObjeto(classe);
  rType := rContext.GetType(classe);
  for field in FQuery.getQueryPrincipal().Fields do
  begin
    rField := rType.GetField(TORMContextoDatabase.Current.GetMeta.obterFieldPorNomeCampo(classe, AnsiLowerCase(field.DisplayName)));

    if (rField <> nil) then
    begin
      obterValue(classe, field, rField, Result, value);

      rField.SetValue(TObject(Result), value);
    end;
  end;
end;

function TORMRtti._FloatFormat(valor: String): Currency;
begin
  while Pos('.', valor) > 0 do
    delete(valor,Pos('.', valor),1);

  Result := StrToCurr(valor);
end;

function TORMRtti.obterValorTipoNulo(value: TValue; rttiRecord: TRttiRecordType): Variant;
var
  rValorField: TRttiField;
  rPossuiValorField: TRttiField;
  vPossuiValor: TValue;
begin
  rValorField := rttiRecord.GetField('FValue');
  rPossuiValorField := rttiRecord.GetField('FPossuiValor');
  vPossuiValor := rPossuiValorField.GetValue(value.GetReferenceToRawData);

  if (vPossuiValor.AsType<Boolean>) then
    Result := obterValorFieldToVariant(rValorField, rValorField.GetValue(value.GetReferenceToRawData))
  else
    Result := Null;
end;

function TORMRtti.obterValorFieldToVariant(rField: TRttiField; value: TValue): Variant;
begin
  case rField.FieldType.TypeKind of
    tkUnknown, tkString, tkWChar, tkLString, tkWString, tkUString:
      Result := value.AsType<String>;
    tkInteger, tkInt64:
      Result := value.AsType<Integer>;
    tkFloat:
    begin
      if (value.TypeInfo = TypeInfo(TDateTime)) then
        Result := StrToDateTime(value.ToString)
      else
      if (value.TypeInfo = TypeInfo(TDate)) then
        Result := StrToDate(value.ToString)
      else
      if (value.TypeInfo = TypeInfo(TTime)) then
        Result := StrToTime(value.ToString)
      else
        Result := value.AsType<Double>;
    end;
    tkVariant:
      Result := value.AsType<Variant>;
  end;
end;

function TORMRtti.obterValorTipoRelacionamento(nomeCampo: String; value: TValue;
  rttiRecord: TRttiRecordType): Variant;
var
  rValorField: TRttiField;
  objeto: TObject;
begin
  rValorField := rttiRecord.GetField('FValue');
  objeto := rValorField.GetValue(value.GetReferenceToRawData).AsObject;

  if (objeto <> nil) then
    Result := obterValorTipoRelacionamentoVariant(nomeCampo, objeto)
  else
    Result := Null;
end;

function TORMRtti.obterValorTipoRelacionamentoVariant(const nomeCampoRelacionamento: String;
  objeto: TObject): Variant;
var
  rContext: TRttiContext;
  rType: TRttiType;
  rField: TRttiField;
  nomeCampo: String;
begin
  Result := Null;
  rType := rContext.GetType(objeto.ClassType);

  for rField in rType.GetFields do
  begin
    nomeCampo := TORMContextoDatabase.Current.GetMeta.obterNomeCampoPorField(objeto.ClassType, rField.Name);

    if (nomeCampo = nomeCampoRelacionamento) then
    begin
      Result := obterValorFieldToVariant(rField, rField.GetValue(TObject(objeto)));
      break;
    end;
  end;
end;

procedure TORMRtti.obterValue(classe: TClass; field: TField; rField: TRttiField;
  var Result: TObject; var value: TValue);
var
  rContext: TRttiContext;
begin
  case rField.FieldType.TypeKind of
    tkUnknown, tkString, tkWChar, tkLString, tkWString, tkUString:
      value := TValue.From<String>(field.AsString);
    tkInteger, tkInt64:
      value := TValue.From<Integer>(field.AsInteger);
    tkChar:
      ;
    tkEnumeration:
      ;
    tkFloat:
      value := TValue.From<Double>(field.AsFloat);
    tkSet:
      ;
    tkClass:
      ;
    tkMethod:
      ;
    tkVariant:
      ;
    tkArray:
      ;
    tkInterface:
      ;
    tkDynArray:
      ;
    tkClassRef:
      ;
    tkPointer:
      ;
    tkProcedure:
      ;
    tkRecord:
      begin
        if (isTypeNulo(rContext.GetType(rField.GetValue(TObject(Result)).TypeInfo))) then
          value := setarValorTipoNulo(rField.GetValue(TObject(Result)).TypeInfo, field.AsVariant);

        if (isTypeRelacionamento(rContext.GetType(rField.GetValue(TObject(Result)).TypeInfo))) then
          value := setarValorTipoRelacionamento(classe, AnsiLowerCase(field.DisplayName), rField.GetValue(TObject(Result)).TypeInfo, field.AsVariant);
      end;
  end;
end;

end.
