unit ORM.SQLAtributos;

interface

uses
  System.Generics.Collections;

type
  TORMSQLAtributos = class
  private
    FGerenciador: TObject;
    FClasse: TClass;
    FFields: String;
    FWhere: String;
    FOrderBy: String;
    FGroupBy: String;
    FJoin: String;
    FFirst: Boolean;

  strict protected
    procedure preencher(resultado: TObjectList<TObject>);
  public
    constructor Create(classe: TClass; gerenciadorObjetos: TObject);

    function Fields(sql: String): TORMSQLAtributos;
    function Where(sql: String): TORMSQLAtributos;
    function OrderBy(sql: String): TORMSQLAtributos;
    function GroupBy(sql: String): TORMSQLAtributos;
    function Join(sql: String): TORMSQLAtributos;

    function getFields(): String;
    function getWhere(): String;
    function getOrderBy(): String;
    function getGroupBy(): String;
    function getJoin(): String;
    function getFirst(): Boolean;

    function getClasse(): TClass;

    function pesquisar<E: class>: E;
    function pesquisarLista<E: class>: TObjectList<E>;
    function count(): Integer;
    function having(): Integer;
    function first<E: class>: E;
  end;

  TORMSQLAtributos<E: class> = class(TORMSQLAtributos)
    function Fields(sql: String): TORMSQLAtributos<E>;
    function Where(sql: String): TORMSQLAtributos<E>;
    function OrderBy(sql: String): TORMSQLAtributos<E>;
    function GroupBy(sql: String): TORMSQLAtributos<E>;
    function Join(sql: String): TORMSQLAtributos<E>;
    function pesquisar(): E;
    function pesquisarLista(): TObjectList<E>;
    function first(): E;
  end;

implementation

uses
  ORM.Gerenciador, System.SysUtils;

type
  TGerenciadorInterno = class(TORMGerenciador);

  { TORMSQLAtributos<E> }

function TORMSQLAtributos.count: Integer;
begin
  try
    Result := TGerenciadorInterno(FGerenciador).count(Self);
  finally
    Self.Free;
  end;
end;

constructor TORMSQLAtributos.Create(classe: TClass;
  gerenciadorObjetos: TObject);
begin
  Assert(gerenciadorObjetos is TORMGerenciador);
  FGerenciador := gerenciadorObjetos;
  FClasse := classe;
  FFirst := False;
end;

function TORMSQLAtributos.Fields(sql: String): TORMSQLAtributos;
begin
  Result := Self;
  if Trim(sql) <> '' then
    FFields := FFields + ' ' + sql;
end;

function TORMSQLAtributos.first<E>: E;
begin
  FFirst := True;
  Result := pesquisar<E>;
end;

function TORMSQLAtributos.getClasse: TClass;
begin
  Result := FClasse;
end;

function TORMSQLAtributos.getFields: String;
begin
  Result := FFields;
end;

function TORMSQLAtributos.getFirst: Boolean;
begin
  Result := FFirst;
end;

function TORMSQLAtributos.getGroupBy: String;
begin
  Result := FGroupBy;
end;

function TORMSQLAtributos.getJoin: String;
begin
  Result := FJoin;
end;

function TORMSQLAtributos.getOrderBy: String;
begin
  Result := FOrderBy;
end;

function TORMSQLAtributos.getWhere: String;
begin
  Result := FWhere;
end;

function TORMSQLAtributos.GroupBy(sql: String): TORMSQLAtributos;
begin
  Result := Self;
  if Trim(sql) <> '' then
    FGroupBy := FGroupBy + ' ' + sql;
end;

function TORMSQLAtributos.having: Integer;
begin
  try
    Result := TGerenciadorInterno(FGerenciador).having(Self);
  finally
    Self.Free;
  end;
end;

function TORMSQLAtributos.Join(sql: String): TORMSQLAtributos;
begin
  Result := Self;
  if Trim(sql) <> '' then
    FJoin := FJoin + ' ' + sql;
end;

function TORMSQLAtributos.OrderBy(sql: String): TORMSQLAtributos;
begin
  Result := Self;
  if Trim(sql) <> '' then
    FOrderBy := FOrderBy + ' ' + sql;
end;

function TORMSQLAtributos.pesquisar<E>: E;
var
  Results: TObjectList<TObject>;
  i: Integer;
begin
  Result := nil;
  try
    Results := TObjectList<TObject>.Create(False);
    try
      preencher(Results);
      if (Results.Count = 0) then
        Exit(nil);

      if (Results.Count > 1) then
        raise Exception.Create(Results.Count.ToString);

      Result := E(Results[0]);
    except
      for i := 0 to Results.Count-1 do
      begin
        Results[i].Free;
        Results[i] := nil;
      end;

      Results.Free;
      raise;
    end;
  finally
    Results.Free;
    Results := nil;

    Self.Free;
  end;
end;

function TORMSQLAtributos.pesquisarLista<E>: TObjectList<E>;
begin
  try
    Result := TObjectList<E>.Create;
    try
      preencher(TObjectList<TObject>(Result));
    except
      Result.Free;
      raise;
    end;
  finally
    Self.Free;
  end;
end;

procedure TORMSQLAtributos.preencher(resultado: TObjectList<TObject>);
begin
  TGerenciadorInterno(FGerenciador).listar(Self, resultado);
end;

function TORMSQLAtributos.Where(sql: String): TORMSQLAtributos;
begin
  Result := Self;
  if Trim(sql) <> '' then
    FWhere := FWhere + ' ' + sql;
end;

{ TORMSQLAtributos<E> }

function TORMSQLAtributos<E>.Fields(sql: String): TORMSQLAtributos<E>;
begin
  inherited Fields(sql);
  Result := Self;
end;

function TORMSQLAtributos<E>.first: E;
begin
  Result := inherited first<E>;
end;

function TORMSQLAtributos<E>.GroupBy(sql: String): TORMSQLAtributos<E>;
begin
  inherited GroupBy(sql);
  Result := Self;
end;

function TORMSQLAtributos<E>.Join(sql: String): TORMSQLAtributos<E>;
begin
  inherited Join(sql);
  Result := Self;
end;

function TORMSQLAtributos<E>.OrderBy(sql: String): TORMSQLAtributos<E>;
begin
  inherited OrderBy(sql);
  Result := Self;
end;

function TORMSQLAtributos<E>.pesquisar: E;
begin
  Result := inherited pesquisar<E>;
end;

function TORMSQLAtributos<E>.pesquisarLista: TObjectList<E>;
begin
  Result := inherited pesquisarLista<E>;
end;

function TORMSQLAtributos<E>.Where(sql: String): TORMSQLAtributos<E>;
begin
  inherited Where(sql);
  Result := Self;
end;

end.
