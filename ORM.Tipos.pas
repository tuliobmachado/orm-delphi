unit ORM.Tipos;

interface

type
  TNuloRecord = record

  end;

  Nulo<T> = record
  private
    FValue: T;
    FPossuiValor: Boolean;
    class function getVazio: Nulo<T>; static;
    function getIsNull: Boolean;
    function getValueOrDefault: T;
    function getValue: T;
    procedure setValue(const valor: T);
    procedure Limpar;

  public
    constructor Create(valor: T);

    property PossuiValor: Boolean read FPossuiValor;
    property isNull: Boolean read getIsNull;
    property Value: T read getValue write setValue;
    property ValueOrDefault: T read getValueOrDefault;

    class property vazio: Nulo<T> read getVazio;

    class operator Implicit(valor: TNuloRecord): Nulo<T>;

    class operator Implicit(valor: T): Nulo<T>;
    class operator Implicit(valor: Nulo<T>): T;
  end;

  Relacionamento<T: class> = record
  private
    FValue: T;
//    FController: IProxyController;
//    FLoaded: Boolean;
    function getValue: T;
    procedure setValue(const valor: T);
//    function GetAvailable: boolean;
//    function GetKey: Variant;
  public
//    procedure Load;
//    procedure SetInitialValue(AValue: T);
//    procedure DestroyValue;
//
//    class operator Equal(Left, Right: Proxy<T>): Boolean;
//    class operator NotEqual(Left, Right: Proxy<T>): Boolean;
//
//    property Available: boolean read GetAvailable;
    property Value: T read getValue write setValue;
//    property Key: Variant read GetKey;
  end;

var
  VNull: TNuloRecord;

implementation

uses
  System.SysUtils, ORM.Exceptions;

{ Nulo<T> }

constructor Nulo<T>.Create(valor: T);
begin
  FPossuiValor := False;
  FValue := valor;
end;

class function Nulo<T>.getVazio: Nulo<T>;
begin
  Result.Limpar;
end;

function Nulo<T>.getIsNull: Boolean;
begin
  Result := not FPossuiValor;
end;

function Nulo<T>.getValueOrDefault: T;
begin
  if (FPossuiValor) then
    Result := FValue
  else
    Result := Default(T);
end;

function Nulo<T>.getValue: T;
begin
  if (not FPossuiValor) then
    raise ENuloValorException<T>.Create;

  Result := FValue;
end;

class operator Nulo<T>.Implicit(valor: TNuloRecord): Nulo<T>;
begin
  Result.FPossuiValor := False;
end;

class operator Nulo<T>.Implicit(valor: T): Nulo<T>;
begin
  Result.FValue := valor;
  Result.FPossuiValor := True;
end;

class operator Nulo<T>.Implicit(valor: Nulo<T>): T;
begin
  Result := valor.getValue;
end;

procedure Nulo<T>.Limpar;
begin
  FPossuiValor := False;
  FValue := Default(T);
end;

procedure Nulo<T>.setValue(const valor: T);
begin
  FValue := valor;
  FPossuiValor := True;
end;

{ Relacionamento<T> }

function Relacionamento<T>.GetValue: T;
begin
  Result := FValue;
end;

procedure Relacionamento<T>.SetValue(const valor: T);
begin
  FValue := valor;
end;

end.
